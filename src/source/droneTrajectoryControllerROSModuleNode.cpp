//////////////////////////////////////////////////////
//  droneTrajectoryControllerROSModuleNode.cpp
//
//  Created on: Dec 11, 2013
//      Author: jespestana
//
//  Last modification on: Dec 11, 2013
//      Author: jespestana
//
//////////////////////////////////////////////////////

#include <iostream>
#include <math.h>

#include "ros/ros.h"
#include "droneTrajectoryControllerROSModule.h"
#include "nodes_definition.h"

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, MODULE_NAME_TRAJECTORY_CONTROLLER);
    ros::NodeHandle n;

    std::cout << "[ROSNODE] Starting "<<ros::this_node::getName() << std::endl;

    DroneTrajectoryControllerROSModule MyDroneTrajectoryController;
    MyDroneTrajectoryController.open(n);

    try
    {
        while(ros::ok())
        {
            //Read messages
            ros::spinOnce();

            //Run EKF State Estimator
            if(MyDroneTrajectoryController.run())
            {
            }
            //Sleep
            MyDroneTrajectoryController.sleep();
        }
    }
    catch (std::exception &ex)
    {
        std::cout << "[ROSNODE] Exception :" << ex.what() << std::endl;
    }
    return 0;
}

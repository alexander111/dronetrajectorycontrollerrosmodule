#ifndef DRONETRAJECTORYCONTROLLERROSMODULE_H
#define DRONETRAJECTORYCONTROLLERROSMODULE_H

#include <string>

#include "ros/ros.h"
#include "droneModuleROS.h"
#include "communication_definition.h"
#include "droneTrajectoryController.h"
//Drone msgs
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/droneAltitudeCmd.h"
//Services
//#include "droneTrajectoryControllerROSModule/setControlMode.h"
#include "droneMsgsROS/setControlMode.h"
#include "droneloggerrospublisher.h"

// CVG utils
#include "xmlfilereader.h"
#include "cvg_string_conversions.h"

class DroneTrajectoryControllerROSModule : public DroneModule
{
public:
    DroneTrajectoryControllerROSModule();
    ~DroneTrajectoryControllerROSModule();

    // DroneModule stuff: reset, start, stop, run, init, open, close
private:
    void init();
    void close();
protected:
    bool resetValues();
    bool startVal();
    bool stopVal();
public:
    bool run();
    void open(ros::NodeHandle & nIn);
protected:
    void readParameters();
public:
    bool readConfigs(std::string configFile);

private:
    DroneTrajectoryController drone_trajectory_controller;
    bool setControlModeVal(Controller_MidLevel_controlMode::controlMode mode);


private:
    std::string drone_trajectory_controller_config_file;

    // [Subscribers] Controller references
private:
    std::string dronePositionRefTopicName;
    ros::Subscriber dronePositionRefSub;
    void dronePositionRefsSubCallback(const droneMsgsROS::dronePositionRefCommandStamped::ConstPtr &msg);

    std::string droneSpeedsRefTopicName;
    ros::Subscriber droneSpeedsRefSub;
    void droneSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg);

    std::string droneTrajectoryAbsRefTopicName;
    ros::Subscriber droneTrajectoryAbsRefSub;  // Trajectory specified in world coordinates
    void droneTrajectoryAbsRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg);

    std::string droneTrajectoryRelRefTopicName;
    ros::Subscriber droneTrajectoryRelRefSub;  // Trajectory specified relative to current drone pose
    void droneTrajectoryRelRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg);
    bool stay_in_position;

    std::string droneYawRefCommandTopicName;
    ros::Subscriber droneYawRefCommandSub;
    void droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr& msg);

    // PBVS, Altitude commands
    std::string dronePBVSAltitudeRefCommandTopicName;
    ros::Subscriber dronePBVSAltitudeRefCommandSub;
    void dronePBVSAltitudeRefCommandCallback(const droneMsgsROS::droneAltitudeCmd::ConstPtr& msg);

    // [Publishers] Controller references, meaning of useful values depend on the current control mode
    // useful values: x, y, z, yaw
    std::string drone_position_reference_topic_name;
    ros::Publisher drone_position_reference_publisher;
    droneMsgsROS::dronePose   current_drone_position_reference;

    // useful values: vxfi, vyfi, vzfi (dyawfi is unused, undebugged)
    std::string drone_speed_reference_topic_name;
    ros::Publisher drone_speed_reference_publisher;
    droneMsgsROS::droneSpeeds current_drone_speed_reference;

    // useful values: current trajectory: initial_checkpoint,isPeridioc, checkpoints[]
    std::string drone_trajectory_reference_topic_name;
    ros::Publisher drone_trajectory_reference_publisher;
    droneMsgsROS::dronePositionTrajectoryRefCommand current_drone_trajectory_command;

    void publishControllerReferences();

    // [Subscribers] State estimation: controller feedback inputs
private:
    std::string droneEstimatedPoseTopicName;
    ros::Subscriber droneEstimatedPoseSubs;
    void droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg);
    droneMsgsROS::dronePose last_estimatedPose;

    std::string droneEstimatedSpeedsTopicName;
    ros::Subscriber droneEstimatedSpeedsSubs;
    void droneEstimatedSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr& msg);
    droneMsgsROS::droneSpeeds last_estimatedSpeed;

    //PBVS, Position Based Visual Servoing
    std::string dronePBVSEstimatedRelPoseTopicName;
    ros::Subscriber dronePBVSEstimatedRelPoseSubs;
    void dronePBVSEstimatedRelPoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg);
    droneMsgsROS::dronePose last_PBVSestimatedRelPose;

    // Publishers
private:
    std::string controlModeTopicName;
    ros::Publisher controlModePub;
    void publishControlMode();

    std::string drone_command_pitch_roll_topic_name;
    ros::Publisher drone_command_pitch_roll_publisher;
    droneMsgsROS::dronePitchRollCmd drone_command_pitch_roll_msg;

    std::string drone_command_dyaw_topic_name;
    ros::Publisher drone_command_dyaw_publisher;
    droneMsgsROS::droneDYawCmd      drone_command_dyaw_msg;

    std::string drone_command_daltitude_topic_name;
    ros::Publisher drone_command_daltitude_publisher;
    droneMsgsROS::droneDAltitudeCmd drone_command_daltitude_msg;


    int publishDroneNavCommand(void);
    void setNavCommand(float pitch, float roll, float dyaw, float dz, double time=-1.0);
    void setNavCommandToZero();


    // Service servers
private:
    std::string setControlModeServiceName;
    ros::ServiceServer setControlModeServerSrv;
    bool setControlModeServCall(droneMsgsROS::setControlMode::Request& request, droneMsgsROS::setControlMode::Response& response);

    // Drone Logger
    DroneLoggerROSPublisher drone_logger_ros_publisher;
};

#endif // DRONETRAJECTORYCONTROLLERROSMODULE_H
